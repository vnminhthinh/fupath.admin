<div class="page-content" style="font-family: 'Arial Unicode MS'; text-transform: capitalize;">
  <div class="page-header">
    <h1>
      Administration
      <small>
        <i class="ace-icon fa fa-angle-double-right"></i>
        Information
      </small>
    </h1>
  </div>
  <!-- /.page-header -->
  <div class="row">
    <div class="col-xs-12">
      <!-- PAGE CONTENT BEGINS -->

      <div class="row">
        <div class="col-xs-12">

          <h3 class="header smaller lighter blue">Administrator</h3>

          <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
          </div>
          <div class="table-header">
            Results for "Administrator Information"
          </div>

          <!-- div.table-responsive -->

          <!-- div.dataTables_borderWrap -->
          <div>
            <table id="customer-table" class="table table-striped table-bordered table-hover">
              <thead>
              <tr>
                <th class="center">
                  <label class="pos-rel">
                    <input type="checkbox" class="ace"/>
                    <span class="lbl"></span>
                  </label>
                </th>
                <th>Name</th>
                <th>Birthday</th>
                <th>Phone</th>
                <th>Email</th>
              </tr>
              </thead>

              <tbody>
              <#if customerInfoList ??>
                <#list customerInfoList as info>
                  <tr>
                    <td class="center">
                      <label class="pos-rel">
                        <input type="checkbox" class="ace"/>
                        <span class="lbl"></span>
                      </label>
                    </td>

                    <td>${info.name}</td>
                    <td>${info.birthday}</td>
                    <td>${info.phone}</td>
                    <td><a href="mailto:${info.email}">${info.email}</td>
                  </tr>
                </#list>
              </#if>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <!-- PAGE CONTENT ENDS -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</div>

<script type="text/javascript">
  let customerTable =
      $('#customer-table')
      //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
      .DataTable({
        bAutoWidth: false,
        "aoColumns": [
          {"bSortable": false},
          null, null, null, null
        ],
        "aaSorting": [],
        select: {
          style: 'multi'
        }
      });

  $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

  new $.fn.dataTable.Buttons(customerTable, {
    buttons: [
      {
        "extend": "colvis",
        "text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
        "className": "btn btn-white btn-primary btn-bold",
        columns: ':not(:first):not(:last)'
      },
      {
        "extend": "copy",
        "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
        "className": "btn btn-white btn-primary btn-bold"
      },
      {
        "extend": "csv",
        "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
        "className": "btn btn-white btn-primary btn-bold"
      },
      {
        "extend": "excel",
        "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
        "className": "btn btn-white btn-primary btn-bold"
      },
      {
        "extend": "pdf",
        "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
        "className": "btn btn-white btn-primary btn-bold"
      },
      {
        "extend": "print",
        "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
        "className": "btn btn-white btn-primary btn-bold",
        autoPrint: false,
        message: 'This print was produced using the Print button for DataTables'
      }
    ]
  });
  customerTable.buttons().container().appendTo($('.tableTools-container'));

  //style the message box
  let defaultCopyAction = customerTable.button(1).action();
  customerTable.button(1).action(function (e, dt, button, config) {
    defaultCopyAction(e, dt, button, config);
    $('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
  });

  let defaultColvisAction = customerTable.button(0).action();
  customerTable.button(0).action(function (e, dt, button, config) {
    defaultColvisAction(e, dt, button, config);
    if ($('.dt-button-collection > .dropdown-menu').length == 0) {
      $('.dt-button-collection')
      .wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
      .find('a').attr('href', '#').wrap("<li />")
    }
    $('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
  });

  ////

  setTimeout(function () {
    $($('.tableTools-container')).find('a.dt-button').each(function () {
      var div = $(this).find(' > div').first();
      if (div.length == 1) {
        div.tooltip({container: 'body', title: div.parent().text()});
      } else {
        $(this).tooltip({container: 'body', title: $(this).text()});
      }
    });
  }, 500);

  customerTable.on('select', function (e, dt, type, index) {
    if (type === 'row') {
      $(customerTable.row(index).node()).find('input:checkbox').prop('checked', true);
    }
  });
  customerTable.on('deselect', function (e, dt, type, index) {
    if (type === 'row') {
      $(customerTable.row(index).node()).find('input:checkbox').prop('checked', false);
    }
  });

  /////////////////////////////////
  //table checkboxes
  $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

  //select/deselect all rows according to table header checkbox
  $('#customer-table > thead > tr > th input[type=checkbox], #customer-table_wrapper input[type=checkbox]').eq(
      0).on('click', function () {
    let th_checked = this.checked;//checkbox inside "TH" table header

    $('#customer-table').find('tbody > tr').each(function () {
      let row = this;
      if (th_checked) {
        customerTable.row(row).select();
      } else {
        customerTable.row(row).deselect();
      }
    });
  });

  //select/deselect a row when the checkbox is checked/unchecked
  $('#customer-table').on('click', 'td input[type=checkbox]', function () {
    let row = $(this).closest('tr').get(0);
    if (this.checked) {
      customerTable.row(row).deselect();
    } else {
      customerTable.row(row).select();
    }
  });

  $(document).on('click', '#customer-table .dropdown-toggle', function (e) {
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
  });

</script>