<div class="page-content" style="font-family: 'Arial Unicode MS'; text-transform: capitalize;">
  <div class="page-header">
    <h1>
      Mange Order
      <small>
        <i class="ace-icon fa fa-angle-double-right"></i>
        Recent Orders
      </small>
    </h1>
  </div>
  <!-- /.page-header -->
  <div class="row">
    <div class="col-xs-12">
      <!-- PAGE CONTENT BEGINS -->
      <table cellspacing="10">
        <tbody>
        <tr>
          <th>
            <div style="font-family: 'Arial Unicode MS'; text-transform: capitalize;">Trạng Thái
            </div>
          </th>
          <th>
            <div class="col-xs-12 col-sm-9">
              <select id="stateType" class="multiselect" multiple="">
                <#list orderState as state>
                  <option value="${state.value}" selected>${state.label}</option>
                </#list>
              </select>
            </div>
          </th>
          <th>
          </th>
        </tr>
        </tbody>
      </table>

      <!-- table items -->

      <div class="row">
        <div class="col-xs-12">

          <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
          </div>
          <div class="table-header"
               style="font-family: 'Arial Unicode MS'; text-transform: capitalize;">
            Danh sách đơn hàng
          </div>

          <!-- div.table-responsive -->

          <!-- div.dataTables_borderWrap -->
          <div>
            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
              <thead>
              <tr>
                <th></th>
                <th>Mã đơn hàng</th>
                <th>Khách hàng</th>
                <th>Trạng thái</th>
                <th>Ngày tạo đơn</th>
                <th>Ngày thử</th>
                <th>Ngày giao</th>
                <th>Đặt cọc</th>
                <th>Công nợ</th>
                <th>Tổng đơn hàng</th>
              </tr>
              </thead>

              <tbody>
                <#list csOrderList as order>
                <tr>
                  <td class="center">
                    <label class="pos-rel">
                      <input type="checkbox" class="ace" value="${order.id}"/>
                      <span class="lbl"></span>
                    </label>
                  </td>
                  <td>${order.refId}</td>
                  <td>${order.csName}</td>
                  <td>${order.state}</td>
                  <td>${order.dateCreate}</td>
                  <td>${order.dateTried}</td>
                  <td>${order.dateSend}</td>
                  <td>${order.prepaid}</td>
                  <td>${order.liabilities}</td>
                  <td>${order.total}</td>
                </tr>
                </#list>
              </tbody>
              <tfoot>
              <tr>
                <th colspan="7" style="text-align:right">Total:</th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>

      <div class="space-30"></div>
      <div class="row" id="orderDetail" hidden>
        <div class="col-xs-12">

          <div class="table-header"
               style="font-family: 'Arial Unicode MS'; text-transform: capitalize;">
            Danh sách đơn hàng chi tiết
          </div>

          <!-- div.table-responsive -->

          <!-- div.dataTables_borderWrap -->
          <div>
            <table id="detail-table" class="table table-striped table-bordered table-hover">
              <thead>
              <tr>
                <th style="text-align: center">STT</th>
                <th>Mặt hàng</th>
                <th>Đơn giá</th>
                <th>Số lượng</th>
                <th>Ngày thử</th>
                <th>Ngày nhận</th>
                <th>Thành tiền</th>
                <th>Bổ sung</th>
                <th></th>
              </tr>
              </thead>

              <tbody>
              <tr>
                <td style="text-align: center">xx</td>
                <td>xx</td>
                <td>xx</td>
                <td>xx</td>
                <td>xx</td>
                <td>xx</td>
                <td>xx</td>
                <td>xx</td>
                <td>
                  <div class="hidden-sm hidden-xs action-buttons">
                    <a class="red" href="/removeOrder?id=">
                      <i class="ace-icon fa fa-trash-o bigger-130"></i>
                    </a>
                    <a class="green" href="/doneOrder?id=">
                      <i class="ace-icon fa fa-check bigger-130"></i>
                    </a>
                  </div>
                </td>
              </tr>
              </tbody>
            </table>
          </div>

          <div class="space-30"></div>
          <div class="table-header"
               style="font-family: 'Arial Unicode MS'; text-transform: capitalize;">
            Thông tin khách hàng
          </div>
          <table style="width: 100%;">
            <col width="50%">
            <col width="50%">
            <tbody>
              <tr>
                <td valign="top" style="padding: 5px;">
                  <div class="table-header"
                       style="font-family: 'Arial Unicode MS'; text-transform: capitalize;">
                    Contact
                  </div>
                  <table style="width: 100%; margin-top: 10px;">
                    <tbody>
                    <tr>
                      <th style="text-align: right; padding-bottom: 1em;">Tên: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">Test</td>
                      <th style="text-align: right; padding-bottom: 1em;">Email: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;"><a href="mailto:abc@gmail.com">abc@gmail.com</a></td>
                      <th style="text-align: right; padding-bottom: 1em;">Phone: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">01674 522 710</td>
                    </tr>
                    <tr>
                      <th style="text-align: right;  padding-bottom: 1em;">Ngày sinh: </th>
                      <td style="padding-left: 10px; padding-bottom: 1em;">01-01-2018</td>
                      <th style="text-align: right;  padding-bottom: 1em;">Địa chỉ: </th>
                      <td style="padding-left: 10px; padding-bottom: 1em;">ABC quan 10</td>
                      <th style="text-align: right;  padding-bottom: 1em;">Nghề nghiệp: </th>
                      <td style="padding-left: 10px; padding-bottom: 1em;">Nhan vien</td>
                    </tr>
                    <tr>
                      <th style="text-align: right">Giới tính: </th>
                      <td style="padding-left: 10px;">Nam</td>
                      <th style="text-align: right">Chiều cao: </th>
                      <td style="padding-left: 10px;">1.56</td>
                      <th style="text-align: right">Cân nặng: </th>
                      <td style="padding-left: 10px;">34</td>
                    </tr>
                    </tbody>
                  </table>
                  <div style="margin-top: 20px; padding: 20px;"><b>Notes:</b>
                    <div>...</div>
                  </div>

                </td>
                <td valign="top" style="padding: 5px;">
                  <div class="table-header"
                       style="font-family: 'Arial Unicode MS'; text-transform: capitalize; margin-top: 0px;">
                    Số đo
                  </div>
                  <table style="width: 100%; margin-top: 10px;">
                    <tbody>
                    <tr>
                      <th style="text-align: right; padding-bottom: 1em;">Dài tổng cộng: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                      <th style="text-align: right; padding-bottom: 1em;">Dài trước: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                      <th style="text-align: right; padding-bottom: 1em;">Cổ: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                      <th style="text-align: right; padding-bottom: 1em;">Vai: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                    </tr>
                    <tr>
                      <th style="text-align: right; padding-bottom: 1em;">Nửa Vai: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                      <th style="text-align: right; padding-bottom: 1em;">Tay: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                      <th style="text-align: right; padding-bottom: 1em;">Bắp tay: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                      <th style="text-align: right; padding-bottom: 1em;">Khủy tay: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                    </tr>
                    <tr>
                      <th style="text-align: right; padding-bottom: 1em;">Ngực: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                      <th style="text-align: right; padding-bottom: 1em;">Ngang ngực trên: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                      <th style="text-align: right; padding-bottom: 1em;">Ngang ngực dưới: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                      <th style="text-align: right; padding-bottom: 1em;">Eo: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                    </tr>
                    <tr>
                      <th style="text-align: right; padding-bottom: 1em;">Ngang eo: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                      <th style="text-align: right; padding-bottom: 1em;">Bụng: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                      <th style="text-align: right; padding-bottom: 1em;">Hạ Bụng: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                      <th style="text-align: right; padding-bottom: 1em;">Măng sét: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                    </tr>
                    <tr>
                      <th style="text-align: right; padding-bottom: 1em;">Khác: </th>
                      <td style="padding-left: 10px;padding-bottom: 1em;">0.0</td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <!-- PAGE CONTENT ENDS -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</div>


<script type="text/javascript">
  var myTable =
      $('#dynamic-table')
      //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
      .DataTable({
        bAutoWidth: false,
        "aoColumns": [
          {"bSortable": false},
          null, null, null, null, null, null, null, null, null
        ],
        "aaSorting": [],

        select: {
          style: 'single'
        },

        "footerCallback": function (row, data, start, end, display) {
          var api = this.api(), data;

          // Remove the formatting to get integer data for summation
          var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$,.]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
          };

          // Total over this page
          pageTotal = api
          .column(7, {page: 'current'})
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);

          pageTotal2 = api
          .column(8, {page: 'current'})
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);

          pageTotal3 = api
          .column(9, {page: 'current'})
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);

          // Update footer
          $(api.column(7).footer()).html(
              pageTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
          );
          $(api.column(8).footer()).html(
              pageTotal2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
          );
          $(api.column(9).footer()).html(
              pageTotal3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
          );
        },
      });

  var detailTable =
      $('#detail-table').wrap("<div class='dataTables_borderWrap' />").DataTable({
        bAutoWidth: false,
        "aoColumns": [
          null, null, null, null, null, null, null, null, null
        ]
      });

  $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

  new $.fn.dataTable.Buttons(myTable, {
    buttons: [
      {
        "extend": "colvis",
        "text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
        "className": "btn btn-white btn-primary btn-bold",
        columns: ':not(:first):not(:last)'
      },
      {
        "extend": "copy",
        "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
        "className": "btn btn-white btn-primary btn-bold"
      },
      {
        "extend": "csv",
        "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
        "className": "btn btn-white btn-primary btn-bold"
      },
      {
        "extend": "excel",
        "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
        "className": "btn btn-white btn-primary btn-bold"
      },
      {
        "extend": "pdf",
        "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
        "className": "btn btn-white btn-primary btn-bold"
      },
      {
        "extend": "print",
        "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
        "className": "btn btn-white btn-primary btn-bold",
        autoPrint: false,
        message: 'This print was produced using the Print button for DataTables'
      }
    ]
  });
  myTable.buttons().container().appendTo($('.tableTools-container'));

  //style the message box
  var defaultCopyAction = myTable.button(1).action();
  myTable.button(1).action(function (e, dt, button, config) {
    defaultCopyAction(e, dt, button, config);
    $('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
  });

  var defaultColvisAction = myTable.button(0).action();
  myTable.button(0).action(function (e, dt, button, config) {

    defaultColvisAction(e, dt, button, config);

    if ($('.dt-button-collection > .dropdown-menu').length == 0) {
      $('.dt-button-collection')
      .wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
      .find('a').attr('href', '#').wrap("<li />")
    }
    $('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
  });

  ////

  setTimeout(function () {
    $($('.tableTools-container')).find('a.dt-button').each(function () {
      var div = $(this).find(' > div').first();
      if (div.length == 1) {
        div.tooltip({container: 'body', title: div.parent().text()});
      } else {
        $(this).tooltip({container: 'body', title: $(this).text()});
      }
    });
  }, 500);

  myTable.on('select', function (e, dt, type, index) {
    if (type === 'row') {
      $(myTable.row(index).node()).find('input:checkbox').prop('checked', true);
      $("#orderDetail").show();
      detailTable.clear();
      detailTable.draw();

      let id = $(myTable.row(index).node()).find('input:checkbox').val();

      $.get("/getOrderDetail?id=" + id).done(function (data) {
        let orderDetails = JSON.parse(data);
        console.log(orderDetails);

        for (let i in orderDetails) {
          let od = orderDetails[i];
          let o1 = od.stt;
          let o2 = od.matHang;
          let o3 = od.donGia;
          let o4 = od.soLuong;
          let o5 = od.ngayThu;
          let o6 = od.ngayNhan;
          let o7 = od.donGia * od.soLuong;
          let o8 = od.boSung;
          let o9 = "<div class=\"hidden-sm hidden-xs action-buttons\">" +
              "<a class=\"red\" href=\"/removeOrder?id=\">\n"
              + "                      <i class=\"ace-icon fa fa-trash-o bigger-130\"></i>\n"
              + "                    </a>\n"
              + "                    <a class=\"green\" href=\"/doneOrder?id=\">\n"
              + "                      <i class=\"ace-icon fa fa-check bigger-130\"></i>\n"
              + "                    </a>"
          "</div>";
          detailTable.row.add([o1, o2, o3, o4, o5, o6, o7, o8, o9]);
        }
        detailTable.draw();
      });

    }
  });
  myTable.on('deselect', function (e, dt, type, index) {
    if (type === 'row') {
      $(myTable.row(index).node()).find('input:checkbox').prop('checked', false);
      $("#orderDetail").hide();
    }
  });

  /////////////////////////////////
  //table checkboxes
  $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

  //select/deselect a row when the checkbox is checked/unchecked
  $('#dynamic-table').on('click', 'td input[type=checkbox]', function () {
    var row = $(this).closest('tr').get(0);
    if (this.checked) {
      myTable.row(row).deselect();
    } else {
      myTable.row(row).select();
    }
  });

  $(document).on('click', '#dynamic-table .dropdown-toggle', function (e) {
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
  });

  //And for the first simple table, which doesn't have TableTools or dataTables
  //select/deselect all rows according to table header checkbox
  // var active_class = 'active';
  // $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function () {
  //   var th_checked = this.checked;//checkbox inside "TH" table header
  //
  //   $(this).closest('table').find('tbody > tr').each(function () {
  //     var row = this;
  //     if (th_checked) {
  //       $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop(
  //           'checked', true);
  //     } else {
  //       $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked',
  //           false);
  //     }
  //   });
  // });

  // //select/deselect a row when the checkbox is checked/unchecked
  // $('#simple-table').on('click', 'td input[type=checkbox]', function () {
  //   var $row = $(this).closest('tr');
  //   if ($row.is('.detail-row ')) {
  //     return;
  //   }
  //   if (this.checked) {
  //     $row.addClass(active_class);
  //   } else {
  //     $row.removeClass(active_class);
  //   }
  // });

  /********************************/
  //add tooltip for small view action buttons in dropdown menu
  $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

  //tooltip placement on right or left
  function tooltip_placement(context, source) {
    var $source = $(source);
    var $parent = $source.closest('table')
    var off1 = $parent.offset();
    var w1 = $parent.width();

    var off2 = $source.offset();
    //var w2 = $source.width();

    if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2)) {
      return 'right';
    }
    return 'left';
  }

  /***************/
  $('.show-details-btn').on('click', function (e) {
    e.preventDefault();
    $(this).closest('tr').next().toggleClass('open');
    $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass(
        'fa-angle-double-up');
  });
  /***************/


</script>

<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<!-- page specific plugin scripts -->
<script src="assets/js/jquery.bootstrap-duallistbox.min.js"></script>
<script src="assets/js/jquery.raty.min.js"></script>
<script src="assets/js/bootstrap-multiselect.min.js"></script>
<script src="assets/js/select2.min.js"></script>
<script src="assets/js/jquery-typeahead.js"></script>

<!-- ace scripts -->
<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
  var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox(
      {infoTextFiltered: '<span class="label label-purple label-lg">Filtered</span>'});
  var container1 = demo1.bootstrapDualListbox('getContainer');
  container1.find('.btn').addClass('btn-white btn-info btn-bold');

  /**var setRatingColors = function() {
					$(this).find('.star-on-png,.star-half-png').addClass('orange2').removeClass('grey');
					$(this).find('.star-off-png').removeClass('orange2').addClass('grey');
				}*/
  $('.rating').raty({
    'cancel': true,
    'half': true,
    'starType': 'i'
    /**,

     'click': function() {
						setRatingColors.call(this);
					},
     'mouseover': function() {
						setRatingColors.call(this);
					},
     'mouseout': function() {
						setRatingColors.call(this);
					}*/
  })//.find('i:not(.star-raty)').addClass('grey');

  //////////////////
  //select2
  $('.select2').css('width', '200px').select2({allowClear: true})
  $('#select2-multiple-style .btn').on('click', function (e) {
    var target = $(this).find('input[type=radio]');
    var which = parseInt(target.val());
    if (which == 2) {
      $('.select2').addClass('tag-input-style');
    } else {
      $('.select2').removeClass('tag-input-style');
    }
  });

  //////////////////
  $('.multiselect').multiselect({
    enableFiltering: true,
    enableHTML: true,
    buttonClass: 'btn btn-white btn-primary',
    templates: {
      button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span> &nbsp;<b class="fa fa-caret-down"></b></button>',
      ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
      li: '<li><a tabindex="0"><label></label></a></li>',
      divider: '<li class="multiselect-item divider"></li>',
      liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
    }
  });

  ///////////////////

  //typeahead.js
  //example taken from plugin's page at: https://twitter.github.io/typeahead.js/examples/
  var substringMatcher = function (strs) {
    return function findMatches(q, cb) {
      var matches, substringRegex;

      // an array that will be populated with substring matches
      matches = [];

      // regex used to determine if a string contains the substring `q`
      substrRegex = new RegExp(q, 'i');

      // iterate through the pool of strings and for any string that
      // contains the substring `q`, add it to the `matches` array
      $.each(strs, function (i, str) {
        if (substrRegex.test(str)) {
          // the typeahead jQuery plugin expects suggestions to a
          // JavaScript object, refer to typeahead docs for more info
          matches.push({value: str});
        }
      });

      cb(matches);
    }
  }

  $('input.typeahead').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  }, {
    name: 'states',
    displayKey: 'value',
    source: substringMatcher(ace.vars['US_STATES']),
    limit: 10
  });

  ///////////////

  //in ajax mode, remove remaining elements before leaving page
  $(document).one('ajaxloadstart.page', function (e) {
    $('[class*=select2]').remove();
    $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox('destroy');
    $('.rating').raty('destroy');
    $('.multiselect').multiselect('destroy');
  });

  $("#stateType").on("change", function () {
    let stateTypes = $(this).multiselect().val();
    myTable.clear();
    if (stateTypes != null) {
      $.get("/fitterCSOrder", {
        data: JSON.stringify(stateTypes)
      }).done(function (data) {
        let orderDatas = JSON.parse(data);
        console.log(orderDatas);
        for (let i in orderDatas) {
          let od = orderDatas[i];
          let o1 = "<div class=\"center\">\n"
              + "<label class=\"pos-rel\">\n"
              + "<input type=\"checkbox\" class=\"ace\"/>\n"
              + "<span class=\"lbl\"></span>\n"
              + "</label>\n"
              + "</div>";
          let o2 = od.refId;
          let o3 = od.csName;
          let o4 = od.state;
          let o5 = od.dateCreate;
          let o6 = od.dateTried;
          let o7 = od.dateSend;
          let o8 = od.prepaid;
          let o9 = od.liabilities;
          let o10 = od.total;
          myTable.row.add([o1, o2, o3, o4, o5, o6, o7, o8, o9, o10]);
        }
        myTable.draw();
      });
    }
    console.log("Change: " + stateTypes);
  })
</script>