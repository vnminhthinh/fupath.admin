package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.Item;
import com.teso.tct.admin.ent.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PageRepositoryProject extends JpaRepository<Project, Integer>{
    Page<Project> findAll(Pageable pageable);
}
