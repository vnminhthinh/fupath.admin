package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.Email;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRepo extends JpaRepository<Email, Long> {

}