package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.Item;
import com.teso.tct.admin.ent.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PageRepositoryTransaction extends JpaRepository<Transaction, Integer>{
    Page<Transaction> findAll(Pageable pageable);
}
