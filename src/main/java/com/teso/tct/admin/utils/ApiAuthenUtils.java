package com.teso.tct.admin.utils;

import com.teso.tct.admin.cache.AdminInfoCA;
import com.teso.tct.admin.config.ConfigInfo;
import com.teso.tct.admin.ent.AdminInfo;
import com.teso.framework.utils.ConvertUtils;

import java.util.stream.IntStream;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;

/**
 * @author Anh TST
 */
public class ApiAuthenUtils {

    public static String getAdminInfo(Authentication authentication, final Model model, String htmlReturn) {
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String email = authentication.getName();
            AdminInfo info = AdminInfoCA.getInstance().getAdminInfo(email);
            model.addAttribute("company_name", ConfigInfo.COMPANY_NAME);
            model.addAttribute("client_id", ConfigInfo.CLIENT_ID);
            model.addAttribute("menu", Boolean.parseBoolean(ConfigInfo.MENU));
            model.addAttribute("transaction",Boolean.parseBoolean(ConfigInfo.TRACSACTION));
            if (info == null) {

                return "login";
            }
            model.addAttribute("name", info.getName());
            model.addAttribute("email", info.getEmail());
            model.addAttribute("avatar", info.getAvatar());
            model.addAttribute("role", info.getRole());
            String rule = info.getPermissionInfo().getRule();
            IntStream.range(0, rule.length()).forEach(i ->
                    model.addAttribute("page_" + i, ConvertUtils.toInt(rule.charAt(i)) > 0)
            );
            return htmlReturn;
        }
        return "login";
    }
}
