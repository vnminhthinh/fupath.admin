package com.teso.tct.admin.service;

import com.teso.tct.admin.db.PageRepository;
import com.teso.tct.admin.db.PageRepositoryTransaction;
import com.teso.tct.admin.ent.Item;
import com.teso.tct.admin.ent.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PageServiceTransaction {

    @Autowired
    PageRepositoryTransaction recordRepository;
    public List<Transaction> findAllTransaction(PageRequest pageRequest){
        Page<Transaction> recordsPage = recordRepository.findAll(pageRequest);
        return recordsPage.getContent();
    }
}
