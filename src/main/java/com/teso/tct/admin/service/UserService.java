/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teso.tct.admin.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.teso.tct.admin.cache.AdminInfoCA;
import com.teso.tct.admin.db.AdminInfoDB;
import com.teso.tct.admin.ent.AdminInfo;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Huy Nguyen
 */
@Service
public class UserService implements UserDetailsService {

  private static final ObjectMapper mapper = new ObjectMapper();

  @Autowired
  private AdminInfoDB adminDB;

  @Override
  public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
    System.out.println(
        "com.teso.cooper.admin.service.UserService.loadUserByUsername() --------------------> "
            + login);
    //load db
    try {
      JsonNode node = mapper.readTree(login);
      ((ObjectNode) node).put("login_type", "LOGIN_BY_GOOGLE");

      String username = node.get("open_name").asText();
      String email = node.get("open_email").asText();
      String avatar = node.get("open_avatar").asText();
      AdminInfo info = adminDB.findByEmail(email);
      if (info == null) {
        return new User(null, "{noop}1", new ArrayList<>());
      }

      if (StringUtils.isBlank(info.getName()) || StringUtils.isBlank(info.getAvatar())) {
        info.setName(username);
        info.setAvatar(avatar);
        adminDB.saveAndFlush(info);
      }

      AdminInfoCA.getInstance().addUserInfo(email, info);

      return new User(info.getEmail(), "{noop}1", new ArrayList<>());

    } catch (IOException e) {
      e.printStackTrace();
      return new User(null, "{noop}1", new ArrayList<>());
    }
  }
}
