package com.teso.tct.admin.api;

import com.teso.tct.admin.config.ConfigInfo;
import com.teso.tct.admin.db.MenuRepo;
import com.teso.tct.admin.ent.Menu;
import com.teso.tct.admin.ent.Project;
import com.teso.tct.admin.model.MMenu;
import com.teso.tct.admin.service.PageService;
import com.teso.tct.admin.service.PageServiceProject;
import com.teso.tct.admin.storage.StorageService;
import com.teso.tct.admin.utils.ApiAuthenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Controller
public class MenuController {
    @Autowired
    private MenuRepo menuRepo;
    @Autowired
    PageService pageService;

    @Autowired
    PageServiceProject pageServiceProject;
    private final StorageService storageService;
    private final String UPLOAD_URL = ConfigInfo.UPLOAD_URL;
    private final String UPLOAD_FOLDER = ConfigInfo.UPLOAD_FOLDER;
    private int PAGE_SIZE = Integer.parseInt(ConfigInfo.PAGE_SIZE);
    public MenuController(StorageService storageService) {
        this.storageService = storageService;
    }

    @RequestMapping("/menus")
    private String menu(Model model) {
        model.addAttribute("page_content", "menus.ftl");
        model.addAttribute("upload_url", UPLOAD_URL);
        List<Menu> list = menuRepo.findAll();
        model.addAttribute("menuList", list);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/add_menu")
    public String addProject(Model model) {
        model.addAttribute("page_content", "add_menu.ftl");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("createMenu")
    public void createMenu( HttpServletRequest request, HttpServletResponse response) throws IOException {

        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        Menu menu = new Menu();
        menu.setId(id);
        if (name != null) {
            menu.setName(name);
        } else {
            menu.setName(name);
        }
        if (description != null) {
            menu.setDescription(description);
        } else {
            menu.setDescription("");
        }
        if (type != null) {
            menu.setType(type);
        } else {
            menu.setType("");
        }
        menuRepo.save(menu);
        response.sendRedirect("/add_menu");
    }

    @RequestMapping("/removeMenu")
    public void removeProject(HttpServletResponse response, @RequestParam(name = "id", defaultValue = "0") String id)
            throws IOException {
        menuRepo.deleteById(id);
        response.sendRedirect("/menus");
    }

    @RequestMapping("/updateMenu")
    public String updateProject(Model model, @RequestParam(name = "id", defaultValue = "0") String id) {
        model.addAttribute("page_content", "update_menu.ftl");
        List<Menu> menus = menuRepo.findAllById(Collections.singleton(id));
        model.addAttribute("menuList", menus);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("/updateMenu")
    public void updateProject(@RequestParam(name = "id", defaultValue = "0") String id,
                              HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        Menu menu = menuRepo.getOne(id);
        if (!description.isEmpty()) {
            menu.setDescription(description);
        }
        if (!name.isEmpty()) {
            menu.setName(name);
        }
        if (!type.isEmpty()) {
            menu.setType(type);
        }

        menuRepo.save(menu);
        response.sendRedirect("/menus");
    }
}
