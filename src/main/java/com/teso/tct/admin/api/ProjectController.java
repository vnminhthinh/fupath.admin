package com.teso.tct.admin.api;

import com.teso.tct.admin.config.ConfigInfo;
import com.teso.tct.admin.db.MenuRepo;
import com.teso.tct.admin.db.ProjectDB;
import com.teso.tct.admin.ent.Menu;
import com.teso.tct.admin.ent.Project;
import com.teso.tct.admin.service.PageService;
import com.teso.tct.admin.service.PageServiceProject;
import com.teso.tct.admin.storage.StorageService;
import com.teso.tct.admin.utils.ApiAuthenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Controller
public class ProjectController {
    @Autowired
    private ProjectDB projectDB;
    @Autowired
    PageService pageService;
    @Autowired
    private MenuRepo menuRepo;
    @Autowired
    PageServiceProject pageServiceProject;

    private final StorageService storageService;
    private final String UPLOAD_URL = ConfigInfo.UPLOAD_URL;
    private final String UPLOAD_FOLDER = ConfigInfo.UPLOAD_FOLDER;
    private int PAGE_SIZE = Integer.parseInt(ConfigInfo.PAGE_SIZE);

    public ProjectController(StorageService storageService) {
        this.storageService = storageService;
    }

    @RequestMapping("/projects")
    public String items(Model model, @RequestParam(name = "page", defaultValue = "1") int page) {
        model.addAttribute("page_content", "project.ftl");
        model.addAttribute("upload_url", UPLOAD_URL);
        List<Project> list = projectDB.findAll();
        List<Project> projectList = pageServiceProject.findAll(PageRequest.of(page - 1, PAGE_SIZE));
        int i = list.size();
        int j = i % PAGE_SIZE;
        int k = i / PAGE_SIZE;
        if (j > 0) {
            k++;
        }
        model.addAttribute("projectList", projectList);
        model.addAttribute("page", k);
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("prePage", page - 1);
        model.addAttribute("currentPage", page);
        model.addAttribute("menu", Boolean.parseBoolean(ConfigInfo.MENU));
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/add_project")
    public String addProject(Model model, HttpServletRequest request,
                             HttpServletResponse response) {
        model.addAttribute("page_content", "add_project.ftl");
        List<Menu> list = menuRepo.findAll();
        model.addAttribute("menuList", list);
        model.addAttribute("menu", Boolean.parseBoolean(ConfigInfo.MENU));
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("createProject")
    public void createProject(Model model, @RequestParam("thumbnail") MultipartFile files, HttpServletRequest request, HttpServletResponse response) throws IOException {

        storageService.store(files);
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setType(type);
        project.setThumbnail(UPLOAD_FOLDER + files.getOriginalFilename());
        projectDB.save(project);
        response.sendRedirect("/add_project");
    }

    @PostMapping("createProjectMenu")
    public void createProjectMenu(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        String menu_id = request.getParameter("menu_id");

        Menu menu = menuRepo.findById(menu_id).get();
        Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setType(type);
        if (Boolean.parseBoolean(ConfigInfo.MENU)) {
            project.setMenu(menu);
        }
        projectDB.save(project);
        response.sendRedirect("/add_project");
    }

    @RequestMapping("/removeProject")
    public void removeProject(HttpServletRequest request, HttpServletResponse response, @RequestParam(name = "id", defaultValue = "0") String id)
            throws IOException {
        projectDB.deleteById(id);
        response.sendRedirect("/projects");
    }

    @RequestMapping("/updateProject")
    public String updateProject(Model model, @RequestParam(name = "id", defaultValue = "0") String id) {
        model.addAttribute("page_content", "update_project.ftl");
        List<Project> projects = projectDB.findAllById(Collections.singleton(id));
        model.addAttribute("projects", projects);
        List<Menu> list = menuRepo.findAll();
        model.addAttribute("menuList", list);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("/updateProject")
    public void updateProject(@RequestParam("thumbnail") MultipartFile files, @RequestParam(name = "id", defaultValue = "0") String id,
                              HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        Project project = projectDB.getOne(id);
        if (!description.isEmpty()) {
            project.setDescription(description);
        } else {
            project.setDescription("");
        }
        if (!name.isEmpty()) {
            project.setName(name);
        } else {
            project.setName("");
        }
        if (!type.isEmpty()) {
            project.setType(type);
        } else {
            project.setType("");
        }
        if (!files.getOriginalFilename().isEmpty()) {
            storageService.store(files);
            project.setThumbnail(UPLOAD_FOLDER + files.getOriginalFilename());
        }
        projectDB.save(project);
        response.sendRedirect("/projects");
    }

    @PostMapping("/updateProjectMenu")
    public void updateProject(@RequestParam(name = "id", defaultValue = "0") String id,
                              HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        String menu_id = request.getParameter("menu_id");
        Menu menu = menuRepo.findById(menu_id).get();
        Project project = projectDB.getOne(id);
        if (!description.isEmpty()) {
            project.setDescription(description);
        } else {
            project.setDescription("");
        }
        if (!name.isEmpty()) {
            project.setName(name);
        } else {
            project.setName("");
        }
        if (!type.isEmpty()) {
            project.setType(type);
        } else {
            project.setType("");
        }
        project.setMenu(menu);
        projectDB.save(project);
        response.sendRedirect("/projects");
    }
}
