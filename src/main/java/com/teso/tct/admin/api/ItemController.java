package com.teso.tct.admin.api;

import com.teso.tct.admin.config.ConfigInfo;
import com.teso.tct.admin.db.*;
import com.teso.tct.admin.ent.*;
import com.teso.tct.admin.model.*;
import com.teso.tct.admin.service.PageService;
import com.teso.tct.admin.service.PageServiceProject;
import com.teso.tct.admin.storage.StorageService;
import com.teso.tct.admin.utils.ApiAuthenUtils;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class ItemController {

    @Autowired
    private ProjectDB projectDB;

    @Autowired
    private ItemDB itemDB;

    @Autowired
    PageService pageService;

    @Autowired
    PageServiceProject pageServiceProject;

    private final StorageService storageService;
    private final String UPLOAD_URL = ConfigInfo.UPLOAD_URL;
    private final String UPLOAD_FOLDER = ConfigInfo.UPLOAD_FOLDER;
    private String KEY = ConfigInfo.KEY_TINYMCE;
    private int PAGE_SIZE = Integer.parseInt(ConfigInfo.PAGE_SIZE);
    private boolean PRICE = Boolean.parseBoolean(ConfigInfo.PRICE);

    @Autowired
    public ItemController(StorageService storageService) {
        this.storageService = storageService;
    }


    @RequestMapping("/items")
    public String orderItems(Model model, @RequestParam(name = "page", defaultValue = "1") int page) {
        model.addAttribute("page_content", "items.ftl");
        List<MProject> projectList = projectDB.findAll().stream().map(p ->
                new MProject(p.getId(), p.getName(), p.getThumbnail(), p.getDescription(), p.getCreatedTime())).collect(Collectors.toList());
        model.addAttribute("projectList", projectList);
        List<Item> list = itemDB.findAll();
        List<Item> itemList = pageService.findAll(PageRequest.of(page - 1, PAGE_SIZE));
        int i = list.size();
        int j = i % PAGE_SIZE;
        int k = i / PAGE_SIZE;
        if (j > 0) {
            k++;
        }
        model.addAttribute("menu", Boolean.parseBoolean(ConfigInfo.MENU));
        model.addAttribute("price", PRICE);
        model.addAttribute("itemList", itemList);
        model.addAttribute("upload_url", UPLOAD_URL);
        model.addAttribute("page", k);
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("prePage", page - 1);
        model.addAttribute("currentPage", page);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/add_item")
    public String addItem(Model model, HttpServletRequest request,
                          HttpServletResponse response) {
        model.addAttribute("page_content", "add_items.ftl");
        List<Project> projects = projectDB.findAll();
        model.addAttribute("projects", projects);
        model.addAttribute("key", KEY);
        model.addAttribute("price", PRICE);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }




    @PostMapping("/createItem")
    public void createItemOrder(Model model, @RequestParam("imgUrl") MultipartFile files, HttpServletRequest request,
                                HttpServletResponse response) throws IOException, ParseException {

        String content = request.getParameter("content");
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String price = request.getParameter("price");
        String project_id = request.getParameter("project_id");
        Project project = projectDB.findById(project_id).get();
        Item item = new Item();
        item.setContent(content);
        item.setDescription(description);
        item.setTitle(title);
        item.setImgUrl(UPLOAD_FOLDER + files.getOriginalFilename());
        item.setProject(project);
        if (PRICE == true) {
            item.setPrice(price);
        }
        if (!files.getOriginalFilename().isEmpty()) {
            storageService.store(files);
            item.setImgUrl(UPLOAD_FOLDER + files.getOriginalFilename());
        }else {
            item.setImgUrl(UPLOAD_FOLDER + "default.png");
        }
        itemDB.save(item);
        response.sendRedirect("/add_item");
    }


    @RequestMapping("/removeItem")
    public void removeItem(HttpServletResponse response, @RequestParam(name = "id", defaultValue = "0") String id)
            throws IOException {
        itemDB.deleteById(Long.valueOf(id));
        response.sendRedirect("/items");
    }

    @RequestMapping("/item-content-detail")
    public String portlolioDetailCatagoly(Model model, @RequestParam(name = "id", defaultValue = "0") Long id) {
        List<Item> item = itemDB.findAllById(Collections.singleton(id));
        model.addAttribute("listItems", item);
        return "content-detail";
    }

    @RequestMapping("/updateItem")
    public String updateItem(Model model, @RequestParam(name = "id", defaultValue = "0") Long id) {
        model.addAttribute("page_content", "update_item.ftl");
        List<Project> projects = projectDB.findAll();
        model.addAttribute("projects", projects);
        List<Item> item = itemDB.findAllById(Collections.singleton(id));
        model.addAttribute("listItems", item);
        model.addAttribute("key", KEY);
        model.addAttribute("price", PRICE);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("/updateItem")
    public void updateItem(@RequestParam("imgUrl") MultipartFile files, @RequestParam(name = "id", defaultValue = "0") Long id,
                           HttpServletRequest request, HttpServletResponse response) throws IOException {
        String content = request.getParameter("content");
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String project_id = request.getParameter("project_id");
        String price = request.getParameter("price");
        Project project = projectDB.findById(project_id).get();
        Item item = itemDB.getOne(id);
        if (!content.isEmpty()) {
            item.setContent(content);
        }
        if (!title.isEmpty()) {
            item.setTitle(title);
        }
        if (!description.isEmpty()) {
            item.setDescription(description);
        }
        if (PRICE == true) {
            if (!price.isEmpty()) {
                item.setPrice(price);
            }
        }
        if (!files.getOriginalFilename().isEmpty()) {
            storageService.store(files);
            item.setImgUrl(UPLOAD_FOLDER + files.getOriginalFilename());
        }else {
            item.setImgUrl(UPLOAD_FOLDER + "default.png");
        }
        item.setProject(project);
        itemDB.save(item);
        response.sendRedirect("/items");
    }

}
