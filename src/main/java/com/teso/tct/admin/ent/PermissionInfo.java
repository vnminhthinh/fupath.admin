package com.teso.tct.admin.ent;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "permission_info")
public class PermissionInfo extends BaseModel{

  private String name;
  private String rule;

  @OneToMany(mappedBy = "permissionInfo")
  private Set<AdminInfo> adminInfoList;

  public PermissionInfo() {
  }

  public PermissionInfo(String name, String rule) {
    this.name = name;
    this.rule = rule;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getRule() {
    return rule;
  }

  public void setRule(String rule) {
    this.rule = rule;
  }

  public Set<AdminInfo> getAdminInfoList() {
    return adminInfoList;
  }

  public void setAdminInfoList(Set<AdminInfo> adminInfoList) {
    this.adminInfoList = adminInfoList;
  }
}
